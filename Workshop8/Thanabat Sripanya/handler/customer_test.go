package handler_test

import (
	"encoding/json"
	"errors"
	"server/handler"
	"server/repository"
	"server/service"

	"io"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
)

func TestGetCustomer(t *testing.T) {
	
	t.Run("success", func(t *testing.T) {
		
		custRepo := repository.NewCustomerRepositoryMock()
		expectedCustomers := []repository.Customer{
			{
				CustomerID:   1,
				CustomerName: "Dollar",
				PhoneNumber:  "100",
				BirthDate:    "5/2/2023",
			},
			{
				CustomerID:   2,
				CustomerName: "Rallod",
				PhoneNumber:  "101",
				BirthDate:    "5/2/2023",
			},
		}
		custRepo.On("GetCustomers").Return(expectedCustomers, nil)

		
		custService := service.NewCustomerService(custRepo)
		custHandler := handler.NewCustomerHandler(custService)

		
		app := fiber.New()
		app.Get("/customer", custHandler.GetCustomers)

		
		req := httptest.NewRequest("GET", "/customer", nil)

		
		res, err := app.Test(req)
		assert.NoError(t, err)
		defer res.Body.Close()

		assert.Equal(t, fiber.StatusOK, res.StatusCode)
		assert.Equal(t, "application/json", res.Header.Get("Content-Type"))

		expectedBody, _ := json.Marshal(expectedCustomers)
		body, _ := io.ReadAll(res.Body)
		assert.JSONEq(t, string(expectedBody), string(body))
	})


	t.Run("error", func(t *testing.T) {
		custRepo := repository.NewCustomerRepositoryMock()
		expectedErr := errors.New("error retrieving customers")
		custRepo.On("GetCustomers").Return([]repository.Customer{}, expectedErr)
	
		custService := service.NewCustomerService(custRepo)
		custHandler := handler.NewCustomerHandler(custService)
	
		app := fiber.New()
		app.Get("/customer", custHandler.GetCustomers)
	
		req := httptest.NewRequest("GET", "/customer", nil)
	
		res, err := app.Test(req)
		assert.NoError(t, err)
		defer res.Body.Close()
	
		assert.Equal(t, fiber.StatusInternalServerError, res.StatusCode)
	
		expectedBody := "Error: unexpected error"
		body, _ := io.ReadAll(res.Body)

		assert.Equal(t, string(expectedBody), string(body))
	})
}
