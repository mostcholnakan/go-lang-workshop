package handler

import (
	"encoding/json"
	"fmt"
	"hexa_pheem/errs"
	"hexa_pheem/service"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type productHandler struct {
	prodSrv service.ProductService
}

func NewProductHandler(prodSrv service.ProductService) productHandler {
	return productHandler{prodSrv: prodSrv}
}

func (h productHandler) NewProduct(w http.ResponseWriter, r *http.Request) {
	ID, _ := strconv.Atoi(mux.Vars(r)["ID"])

	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}
	request := service.NewProductRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}
	response, err := h.prodSrv.NewProduct(ID, request)
	if err != nil {
		handleError(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	ID, _ := strconv.Atoi(mux.Vars(r)["ID"])

	response, err := h.prodSrv.GetProducts(ID)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) GetAllProduct(w http.ResponseWriter, r *http.Request) {
	products, err := h.prodSrv.GetAllProducts()
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(products)

}

func (h productHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["ID"])
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}
	request := service.NewProductRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}
	responses, err := h.prodSrv.GetProducts(productID)
	if err != nil {
		handleError(w, err)
		return
	}
	productAfter, err := json.Marshal(responses)
	if err != nil {
		handleError(w, err)
		return
	}
	response, err := h.prodSrv.UpdateProduct(productID, request)
	if err != nil {
		handleError(w, err)
		return
	}
	
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("content-type", "application/json")
	fmt.Fprintf(w,`{"Data After Change: %s"}`,productAfter)
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	ID, _ := strconv.Atoi(mux.Vars(r)["ID"])

	response, err := h.prodSrv.GetProducts(ID)
	if err != nil {
		handleError(w, err)
		return
	}
	productDel, err := json.Marshal(response)
	if err != nil {
		handleError(w, err)
		return
	}

	err = h.prodSrv.DeleteProduct(ID)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(ID)
	fmt.Fprintf(w, `{"Deleted: %s"}`, productDel)
}
