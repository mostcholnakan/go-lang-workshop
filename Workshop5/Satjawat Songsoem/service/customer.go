package service

import (
	"database/sql"
	"gofiber/errs"
	"gofiber/logs"
	"gofiber/repository"

	"go.uber.org/zap"
)

type customerService struct {
	custRepo repository.CustomerRepository
}

func NewCustomerService(custRepo repository.CustomerRepository) customerService {
	return customerService{custRepo: custRepo}
}

func (s customerService) GetCustomers() ([]CustomerResponse, error) {
	customers, err := s.custRepo.GetCustomers()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	custResponses := []CustomerResponse{}
	for _, customer := range customers {
		custResponse := CustomerResponse{
			CustomerID:   customer.CustomerID,
			CustomerName: customer.CustomerName,
			PhoneNumber:  customer.PhoneNumber,
			BirthDate:    customer.BirthDate,
		}
		custResponses = append(custResponses, custResponse)
	}
	logs.Info("Get Customers", zap.Any("customers", custResponses))
	return custResponses, nil
}

func (s customerService) RemoveCustomer(id int) error {
	err := s.custRepo.RemoveCustomer(id)
	if err != nil {
		logs.Error(err)
		return errs.NewNotfoundError("Customer Not Found")
	}
	logs.Info("Remove Customer  : ", zap.Int("id", id))
	return nil
}

func (s customerService) InsertCustomer(customerResponse CustomerResponse) (int, error) {
	customer := repository.Customer{
		CustomerID:   customerResponse.CustomerID,
		CustomerName: customerResponse.CustomerName,
		PhoneNumber:  customerResponse.PhoneNumber,
		BirthDate:    customerResponse.BirthDate,
	}

	customerID, err := s.custRepo.InsertCustomer(customer)
	if err != nil {

		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Insert Customer  : ", zap.Int("id", customerID))
	return customerID, nil

}

func (s customerService) UpdateCustomer(customerResponse CustomerResponse) (int, error) {
	customer := repository.Customer{
		CustomerID:   customerResponse.CustomerID,
		CustomerName: customerResponse.CustomerName,
		PhoneNumber:  customerResponse.PhoneNumber,
		BirthDate:    customerResponse.BirthDate,
	}

	customerID, err := s.custRepo.UpdateCustomer(customer)
	if err != nil {
		logs.Error(err)
		return 0, errs.NewValidationError("Validation Error")
	}
	logs.Info("Update Customer  : ", zap.Int("id", customerID))
	return customerID, nil
}

func (s customerService) GetCustomer(id int) (*CustomerResponse, error) {
	customer, err := s.custRepo.GetCustomer(id)
	logs.Info("Get Customer ByID : ", zap.Int("id", id))

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotfoundError("Customer Not Found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	custResponse := CustomerResponse{
		CustomerID:   customer.CustomerID,
		CustomerName: customer.CustomerName,
		PhoneNumber:  customer.PhoneNumber,
		BirthDate:    customer.BirthDate,
	}
	logs.Info("Get Customer", zap.Int("id", id), zap.Any("customers", custResponse))
	return &custResponse, nil
}
