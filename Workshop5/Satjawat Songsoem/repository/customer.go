package repository

type Customer struct {
	CustomerID   int    `gorm:"column:id" json:"Customer_id" `
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	BirthDate    string `json:"Birth_date"`
}

type CustomerRepository interface {
	GetCustomers() ([]Customer, error)
	GetCustomer(int) (*Customer, error)
	InsertCustomer(Customer) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(Customer) (int, error)
}
