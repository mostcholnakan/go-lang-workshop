package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"

	//"log"

	"net/http"
	"net/http/httptest"
	//"net/http/httptest"
	"strings"
	"time"

	"github.com/Peerawitptk/go4web/handler"
	"github.com/Peerawitptk/go4web/logs"
	"github.com/Peerawitptk/go4web/repository"
	"github.com/Peerawitptk/go4web/service"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()

	ProductRepository := repository.NewProductRepositoryDB(db)
	ProductRepositoryMock := repository.NewProductRepositoryMock() 	
	_ = ProductRepositoryMock
	ProductService := service.NewProductService(ProductRepository)
	ProductHandler := handler.NewProductHandler(ProductService)

	router := mux.NewRouter()
	router.Use(corsMiddleware)
	router.HandleFunc("/products",ProductHandler.GetProducts).Methods(http.MethodGet)
	router.HandleFunc("/products/{id:[0-9]+}",ProductHandler.GetProduct).Methods(http.MethodGet)
	router.HandleFunc("/products/{id:[0-9]+}",ProductHandler.GetUpdate).Methods(http.MethodPut)
	router.HandleFunc("/products/{id:[0-9]+}",ProductHandler.GetDeleteById).Methods(http.MethodDelete)
	router.HandleFunc("/products",ProductHandler.GetPost).Methods(http.MethodPost)
	
	
	logs.Info("Product service started at port"+viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
	
}

func initConfig(){
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".","_"))

	err := viper.ReadInConfig()
	if err != nil{
		panic(err)
	}
}

func initTimeZone(){
	ict,err := time.LoadLocation("Asia/Bangkok")
	if err != nil{
		panic(err)
	}
	time.Local = ict
}

func initDatabase()*sqlx.DB{
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
	viper.GetString("db.username"),
	viper.GetString("db.password"),
	viper.GetString("db.host"),
	viper.GetInt("db.port"),
	viper.GetString("db.database"),
	)


	var err error
	db, err := sqlx.Open(viper.GetString("db.driver"), dsn)
	if err != nil{
		panic(err)
	}
	db.SetConnMaxIdleTime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db

}
func corsMiddleware(handler http.Handler) http.Handler{
	return http.HandlerFunc(func(w http.ResponseWriter, r*http.Request){
		w.Header().Add("Access-Control-Allow-Origin","*")
		w.Header().Add("Content-Type","applicaiton/json")
		w.Header().Set("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT")
		w.Header().Set("Access-Control-Allow-Headers","Accept, Content-Type, Content-Length, Authorization")
		
		rec := httptest.NewRecorder()
		handler.ServeHTTP(rec, r)
		statusCode := rec.Code

		port := strconv.Itoa(viper.GetInt("app.port"))
		logs.Info(fmt.Sprintf("INFO - %s http://localhost:%s%s | status: %d", r.Method, port, r.URL.Path, statusCode))

		if r.Method == "GET" {
			payload, err := ioutil.ReadAll(rec.Body)
			if err != nil {
				logs.Error("failed to read response payload")
			} else {
				payloadStr := string(payload)
				log.Printf("%s", payloadStr)
			}
		}
		if r.Method == http.MethodPut {
			payload, err := ioutil.ReadAll(rec.Body)
			if err != nil {
				logs.Error("failed to read response payload")
			} else {
				payloadStr := string(payload)
				log.Printf("%s", payloadStr)
			}
		}

		for k, v := range rec.Header() {
			w.Header()[k] = v
		}
		w.WriteHeader(statusCode)
		rec.Body.WriteTo(w)
		handler.ServeHTTP(w, r)

	})

}

