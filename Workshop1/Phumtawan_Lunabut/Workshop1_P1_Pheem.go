package main

import "fmt"

type product struct {
	productName   string
	producePrice  int
	productWeight float64
}

func main() {
	// slide Method
	productList := []product{}
	product1 := product{
		productName: "iPhone 13",
		producePrice: 26900,
		productWeight: 0.21,
	}
	productList = append(productList, product1)
	fmt.Println("Product name: ", product1.productName)
	fmt.Println("price: ", product1.producePrice)
	fmt.Println("Weight: ", product1.productWeight, "kg")
}
