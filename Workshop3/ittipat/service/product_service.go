package service

import (
	"database/sql"
	"ittipat/errs"
	"ittipat/logs"
	"ittipat/repository"
	"time"
)

type productService struct {
	proRepo repository.ProductRepository
}

func NewProductService(proRepo repository.ProductRepository) productService {
	return productService{proRepo: proRepo}
}

func (s productService) GetProducts() ([]ProductResponse, error) {
	products, err := s.proRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	proResponses := []ProductResponse{}
	for _, product := range products {
		proResponse := ProductResponse{
			ProductID:     product.ProductID,
			ProductName:   product.ProductName,
			Price:         product.Price,
			ProductDetail: product.ProductDetail,
			DateCreated:   product.DateCreated,
		}
		proResponses = append(proResponses, proResponse)
	}
	return proResponses, nil
}

func (s productService) GetProduct(id int) (*ProductResponse, error) {
	product, err := s.proRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("customer not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	proResponse := ProductResponse{
		ProductID:     product.ProductID,
		ProductName:   product.ProductName,
		Price:         product.Price,
		ProductDetail: product.ProductDetail,
		DateCreated:   product.DateCreated,
	}
	return &proResponse, nil
}

func (s productService) NewProduct(productID int, request NewProductRequest) (*ProductResponse, error) {

	product := repository.Product{
		ProductName:   request.ProductName,
		ProductDetail: request.ProductDetail,
		Price:         request.Price,
		DateCreated:   time.Now().Format("2006-1-2 15:04:05"),
	}

	newPro, err := s.proRepo.Create(product)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := ProductResponse{
		ProductID:     newPro.ProductID,
		ProductName:   newPro.ProductName,
		Price:         newPro.Price,
		ProductDetail: newPro.ProductDetail,
		DateCreated:   newPro.DateCreated,
	}
	return &response, nil
}

func (s productService) UpdateProduct(productID int, request NewProductRequest) (*ProductResponse, error) {
	// Check if the product exists
	_, err := s.GetProduct(productID)
	if err != nil {
		return nil, err
	}

	product := repository.Product{
		ProductID:     productID,
		ProductName:   request.ProductName,
		ProductDetail: request.ProductDetail,
		Price:         request.Price,
		DateCreated:   time.Now().Format("2006-01-02 15:04:05"),
	}

	updatedProduct, err := s.proRepo.Update(product)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := ProductResponse{
		ProductID:     updatedProduct.ProductID,
		ProductName:   updatedProduct.ProductName,
		Price:         updatedProduct.Price,
		ProductDetail: updatedProduct.ProductDetail,
		DateCreated:   updatedProduct.DateCreated,
	}
	return &response, nil
}

func (s productService) DeleteProduct(productID int) error {
	// Check if the product exists
	_, err := s.proRepo.GetById(productID)
	if err != nil {
		return err
	}

	// Delete the product
	err = s.proRepo.Delete(productID)
	if err != nil {
		return err
	}

	return nil
}
