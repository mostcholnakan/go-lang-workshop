package main

import "fmt"

func add(value1, value2 float32) {
	fmt.Printf("Addition result : %v\n", value1+value2)
}

func sub(value1, value2 float32) {
	fmt.Printf("Subtraction result : %v\n", value1-value2)
}

func mul(value1, value2 float32) {
	fmt.Printf("Multiplication result : %v\n", value1*value2)
}

func div(value1, value2 float32) {
	if value2 == 0 {
		fmt.Println("value2 must not equal 0")
	} else {
		fmt.Printf("Division result : %v\n", value1/value2)
	}
}

func main() {

	var value1 float32
	var value2 float32

	fmt.Print("Enter first number : ")
	fmt.Scanln(&value1)
	fmt.Println("")
	fmt.Print("Enter second number : ")
	fmt.Scanln(&value2)
	fmt.Println("")
	add(value1, value2)
	sub(value1, value2)
	mul(value1, value2)
	div(value1, value2)
}
