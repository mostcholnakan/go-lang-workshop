package main

import "fmt"

func main() {
	row := 1
	for row <= 12 {
		num := 1
		col := 1
		for col <= 12 {
			fmt.Printf("%3d ", num*row)
			num += 1
			col += 1
		}
		fmt.Println()
		row += 1
	}
	fmt.Println()
}
