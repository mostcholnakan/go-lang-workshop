package commands

/*
type GetCustomersCommand struct {
	CustomerID int
}

type GetCustomerCommand struct {
	CustomerID int
}
*/

type InsertCustomerCommand struct {
	CustomerID   string
	CustomerName string
	PhoneNumber  string
	BirthDate    string
}

type RemoveCustomerCommand struct {
	CustomerID string
}

type UpdateCustomerCommand struct {
	CustomerID   string
	CustomerName string
	PhoneNumber  string
	BirthDate    string
}
