package main

import (
	"ThanabatSR/Workshop4/ThanabatSR/handler"
	"ThanabatSR/Workshop4/ThanabatSR/repository"
	"ThanabatSR/Workshop4/ThanabatSR/service"

	"github.com/gofiber/fiber/v2"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
	db, err := gorm.Open("postgres", "user=postgres password=1234 host=localhost port=5432 dbname=postgres sslmode=disable TimeZone=UTC")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	app := fiber.New()

	// Initialize the Postgres Customer Repository
	customerRepo := repository.NewCustomerRepository(db)

	// Initialize the Customer Service
	customerService := service.NewCustomerService(customerRepo)

	// Initialize the RESTful Customer Controller
	customerHandler := handler.NewCustomerHandler(customerService)

	// Add other routes here (POST, PUT, DELETE, etc.)
	app.Get("/customers", customerHandler.GetCustomers)
	app.Get("/customers/:id", customerHandler.GetCustomer)
	app.Post("/customers", customerHandler.InsertCustomer)
	app.Delete("/customers/:id", customerHandler.DeleteCustomer)
	app.Put("/customers/:id", customerHandler.UpdateCustomer)

	app.Listen(":8000")
}
