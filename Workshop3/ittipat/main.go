package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"ittipat/handler"
	"ittipat/logs"
	"ittipat/repository"
	"ittipat/service"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func main() {

	initTimeZone()
	initConfig()
	db := initDatabase()

	productRepositoryDB := repository.NewProductRepositoryDB(db)
	productService := service.NewProductService(productRepositoryDB)
	productHandler := handler.NewProductHandler(productService)

	router := mux.NewRouter()
	router.Use(corsMiddleware)

	router.HandleFunc("/products", productHandler.GetProducts).Methods(http.MethodGet)
	router.HandleFunc("/products/{productID:[0-9]+}", productHandler.GetProduct).Methods(http.MethodGet)
	router.HandleFunc("/products/{productID:[0-9]+}", productHandler.NewProduct).Methods(http.MethodPost)
	router.HandleFunc("/products/{productID:[0-9]+}", productHandler.UpdateProduct).Methods(http.MethodPut)
	router.HandleFunc("/products/{productID:[0-9]+}", productHandler.DeleteProduct).Methods(http.MethodDelete)

	logs.Info("Product service started at port" + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length")

		log.Printf(" %v %v ", r.Method, r.URL.Path)
		var body []byte
		if r.Method != http.MethodGet && r.Method != http.MethodDelete {
			var err error
			body, err = ioutil.ReadAll(r.Body)
			if err != nil {
				log.Printf("Error to request body: %v", err)
			}
			log.Printf("Request body: %v", string(body))
			r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		}

		recorder := httptest.NewRecorder()

		next.ServeHTTP(recorder, r)
		resp := recorder.Result()
		// Copy the response back to the original response writer
		for key, values := range resp.Header {
			for _, value := range values {
				w.Header().Add(key, value)
			}
		}

		w.WriteHeader(resp.StatusCode)
		rbody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("Error to response body: %v", err)
		} else {
			log.Printf("Code: %v Response body: %s", resp.StatusCode, rbody)
			// Write the response body back to the original response writer
			_, err = w.Write(rbody)
			if err != nil {
				log.Printf("Error writing response body: %v", err)
			}
		}
	})
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict

}

func initDatabase() *sqlx.DB {

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.database"),
	)

	db, err := sqlx.Open(viper.GetString("db.driver"), dsn)
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return db
}
