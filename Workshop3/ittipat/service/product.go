package service

type ProductResponse struct {
	ProductID     int     `db:"product_id"`
	ProductName   string  `db:"product_name"`
	Price         float32 `db:"price"`
	ProductDetail string  `db:"product_detail"`
	DateCreated   string  `db:"date_created"`
}

// NewProduct
type NewProductRequest struct {
	ProductName   string  `db:"product_name"`
	Price         float32 `db:"price"`
	ProductDetail string  `db:"product_detail"`
}

type ProductService interface {
	GetProducts() ([]ProductResponse, error)
	GetProduct(int) (*ProductResponse, error)
	NewProduct(int, NewProductRequest) (*ProductResponse, error)
	UpdateProduct(int, NewProductRequest) (*ProductResponse, error)
	DeleteProduct(int) error
}
