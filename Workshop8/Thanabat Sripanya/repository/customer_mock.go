package repository

import "github.com/stretchr/testify/mock"

type customerRepositoryMock struct {
	mock.Mock
}

func NewCustomerRepositoryMock() *customerRepositoryMock {
	return &customerRepositoryMock{}
}

func (m *customerRepositoryMock) GetCustomers() ([]Customer, error) {
	args := m.Called()
	return args.Get(0).([]Customer), args.Error(1)
}

