package service

import (
	"Thanabat/Workshop3/Thanabat/errs"
	"Thanabat/Workshop3/Thanabat/logs"
	"Thanabat/Workshop3/Thanabat/repository"
	"database/sql"

	"go.uber.org/zap"
)

type customerService struct {
	custRepo repository.CustomerRepository
}

func NewCustomerService(custRepo repository.CustomerRepository) customerService {
	return customerService{custRepo: custRepo}
}

func (s customerService) GetCustomers() ([]CustomerResponse, error) {
	customers, err := s.custRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	custResponses := []CustomerResponse{}
	for _, customer := range customers {
		custResponse := CustomerResponse{
			CustomerID: customer.CustomerID,
			Name:       customer.Name,
		}
		custResponses = append(custResponses, custResponse)
	}
	// logs.Debug("Number of customers:", zap.Int("count", len(customers)))
	logs.Info("Get Customers")
	return custResponses, nil
}
func (s customerService) GetCustomer(id int) (*CustomerResponse, error) {
	customer, err := s.custRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			logs.Info("Customer not found ID",zap.Int("id", id))
			return nil, errs.NewNotFoundError("Customer not found!!")
		}
		logs.Error(err)
		return nil, err
	}
	custResponse := CustomerResponse{
		CustomerID: customer.CustomerID,
		Name:       customer.Name,
	}
	logs.Info("Get Customer ID",zap.Int("id", id))
	return &custResponse, nil
}
func (s customerService) DeleteCustomer(id int) error {
	err := s.custRepo.DeleteCustomer(id)
	if err != nil {
		if err == sql.ErrNoRows {
			logs.Info("Customer not found ID",zap.Int("id", id))
			return errs.NewNotFoundError("Customer not found!!")
		}
		logs.Error(err)
		return err
	}
	logs.Info("Delete ID:", zap.Int("id", id))
	return nil
}
func (s customerService) InsertCustomer(req CustomerRequest) (*CustomerResponse, error) {
	customer := repository.Customer{
		CustomerID:  req.CustomerID,
		Name:        req.Name,
		DateCreate:  req.DateCreate,
		PhoneNumber: req.PhoneNumber,
	}
	newCust, err := s.custRepo.InsertCustomer(customer)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	response := CustomerResponse{
		CustomerID: newCust.CustomerID,
		Name:       newCust.Name,
	}
	logs.Info("Insert ID:", zap.Int("id", req.CustomerID))
	return &response, nil
}
func (s customerService) UpdateCustomer(id int, req CustomerRequest) (*CustomerResponse, error) {
	_, err := s.GetCustomer(id)
	if err != nil {
		return nil, err
	}
	customer := repository.Customer{
		CustomerID:  id,
		Name:        req.Name,
		DateCreate:  req.DateCreate,
		PhoneNumber: req.PhoneNumber,
	}
	updatedCust, err := s.custRepo.UpdateCustomer(id, customer)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	response := CustomerResponse{
		CustomerID: updatedCust.CustomerID,
		Name:       updatedCust.Name,
	}
	logs.Info("Update ID:", zap.Int("id", id))
	return &response, nil
}
