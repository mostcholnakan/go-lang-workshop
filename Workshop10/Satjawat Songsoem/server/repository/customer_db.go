package repository

import (
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type customerRepositoryDB struct {
	db *gorm.DB
}

func NewCustomerRepositoryDB(db *gorm.DB) customerRepositoryDB {
	return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) InsertCustomer(customer Customer) (string, error) {
	result := r.db.Create(&customer)
	if result.Error != nil {
		log.Printf("Error inserting customer: %v", result.Error)
		return "", result.Error
	}
	log.Printf("Inserted customer with ID %v", customer.CustomerID)
	return customer.CustomerID, nil
}

func (r customerRepositoryDB) RemoveCustomer(customerID string) error {
	result := r.db.Delete(&Customer{}, customerID)
	if result.Error != nil {
		log.Printf("Error removing customer with ID %v: %v", customerID, result.Error)
		return result.Error
	}
	log.Printf("Removed customer with ID %v", customerID)
	return nil
}

func (r customerRepositoryDB) UpdateCustomer(customer Customer) (string, error) {
	result := r.db.Model(&Customer{}).Where("customer_id = ?", customer.CustomerID).Updates(customer)
	if result.Error != nil {
		log.Printf("Error updating customer with ID %v: %v", customer.CustomerID, result.Error)
		return "", result.Error
	}
	log.Printf("Updated customer with ID %v", customer.CustomerID)
	return customer.CustomerID, nil
}

func (r customerRepositoryDB) GetCustomers() ([]Customer, error) {
	customers := []Customer{}
	result := r.db.Preload(clause.Associations).Find(&customers)
	fmt.Printf("%v/n",customers)

	if result.Error != nil {
		log.Printf("Error getting all customers: %v", result.Error)
		return nil, result.Error
	}
	log.Printf("Retrieved all customers (%d total)", len(customers))
	return customers, nil
}

func (r customerRepositoryDB) GetCustomer(id string) (*Customer, error) {
	customer := Customer{}
	result := r.db.First(&customer, id)
	if result.Error != nil {
		log.Printf("Error getting customer with ID %v: %v", id, result.Error)
		return nil, result.Error
	}
	log.Printf("Retrieved customer with ID %v", id)
	return &customer, nil
}
