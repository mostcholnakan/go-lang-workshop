package main

import "fmt"

type person struct {
	name  string
	age   int
	score float32
}

func newPerson(name string, age int, score float32) *person {
	p := person{name: name}
	p.age = age
	p.score = score
	return &p
}
func main() {
	p := newPerson("John", 20, 80.5)

	name := p.name
	age := p.age
	score := p.score
	fmt.Println("Name:", name)
	fmt.Println("Age:", age)
	fmt.Println("Score:", score)
}
