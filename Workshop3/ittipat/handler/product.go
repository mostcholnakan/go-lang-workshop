package handler

import (
	"encoding/json"
	"fmt"
	"ittipat/errs"
	"ittipat/service"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type productHandler struct {
	proSrv service.ProductService
}

func NewProductHandler(proSrv service.ProductService) productHandler {
	return productHandler{proSrv: proSrv}
}

func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request) {

	products, err := h.proSrv.GetProducts()
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(products)
}

func (h productHandler) GetProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])

	product, err := h.proSrv.GetProduct(productID)
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(product)
}

func (h productHandler) NewProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])

	//
	request := service.NewProductRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	response, err := h.proSrv.NewProduct(productID, request)
	if err != nil {
		handleError(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])

	// Decode request body into a NewProductRequest struct
	request := service.NewProductRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	// Update the product with the given ID
	response, err := h.proSrv.UpdateProduct(productID, request)
	if err != nil {
		handleError(w, err)
		return
	}

	// Respond with the updated product as JSON
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func (h productHandler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])

	// Get the product to be deleted
	product, err := h.proSrv.GetProduct(productID)
	if err != nil {
		handleError(w, err)
		return
	}

	// Encode the product as JSON and store it in a variable to show later
	productDeletion, err := json.Marshal(product)
	if err != nil {
		handleError(w, errs.NewUnexpectedError())
		return
	}

	// Delete the product
	err = h.proSrv.DeleteProduct(productID)
	if err != nil {
		handleError(w, err)
		return
	}

	// Respond with the data you stored in step 2 and step 4
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, "Product successfully deleted")
	fmt.Fprintf(w, `{"product_deletion": %s"}`, productDeletion)
}
