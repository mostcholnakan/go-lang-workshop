package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getAmount(prompt string) int {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(prompt)
		input, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}
		input = strings.TrimSpace(input)
		amount, err := strconv.Atoi(input)
		if err != nil {
			fmt.Printf("%v must be a number\n", prompt)
			continue
		}
		return amount
	}
}

func printTable(matrix [][]int) {
	numCols := len(matrix[0])
	colWidth := 4

	// header
	fmt.Printf("%-*s", colWidth, "")
	for i := 1; i <= numCols; i++ {
		fmt.Printf("%-*d", colWidth, i)
	}
	fmt.Println()

	// rows
	for i, row := range matrix {
		fmt.Printf("%-*d", colWidth, i+1)
		for _, val := range row {
			fmt.Printf("%-*d", colWidth, val)
		}
		fmt.Println()
	}
}

func multiTable(n int) [][]int {
	matrix := make([][]int, 12)
	for i := range matrix {
		matrix[i] = make([]int, n)
	}
	for i := 0; i < 12; i++ {
		for j := 0; j < n; j++ {
			matrix[i][j] = (i + 1) * (j + 1)
		}
	}
	return matrix

}

func main() {
	n := getAmount("Enter range of the multiplication table : ")
	multiTable := multiTable(n) //1-12
	printTable(multiTable)
}
