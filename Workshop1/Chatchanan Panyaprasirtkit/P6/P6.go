package P6

import "fmt"

func MultiplyTable(num int64) {
	for i := 1; i <= 12; i++ {
		for j := 1; j <= int(num); j++ {
			fmt.Printf("%4d", i*j)
		}
		fmt.Println()
	}
}
