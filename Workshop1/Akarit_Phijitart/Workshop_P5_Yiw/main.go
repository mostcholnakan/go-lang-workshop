package main

import (
	"fmt"
)

// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func FindLCMOf2Number(a, b int) int {
	lcmOfAB := a * b / GCD(a, b)
	return lcmOfAB
}

func LCM(arr []int) int {
	result := FindLCMOf2Number(arr[0], arr[1])

	//index 2 to n
	for i := 2; i < len(arr); i++ {
		result = FindLCMOf2Number(result, arr[i])
	}
	return result
}

func main() {
	var arr = []int{2, 4, 6, 8, 10}
	fmt.Println("LCM is:", LCM(arr))
}
