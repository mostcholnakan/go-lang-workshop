package handler

import (
	"net/http"

	"github.com/Akarit2001/workshop3/errs"
)

func handleErr(err error) (int, string) {
	switch e := err.(type) {
	case errs.AppError:
		return e.Code, e.Massage
	default:
		return http.StatusInternalServerError, e.Error()
	}
}
