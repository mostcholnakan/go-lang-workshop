package repository

import (
	"github.com/jmoiron/sqlx"
)

type productRepositoryDB struct {
	db *sqlx.DB
}

func NewProductRepositoryDB(db *sqlx.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) Create(prod Product) (*Product, error) {
	query := "insert into product (id, product_name, price, product_detail, date_created) values (?, ?, ?, ?, ?)"
	result, err := r.db.Exec(
		query,
		prod.ID,
		prod.Product_Name,
		prod.Price,
		prod.Product_Detail,
		prod.Date_Created,
	)
	if err != nil {
		return nil, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	prod.ID = int(id)
	return &prod, nil
}

func (r productRepositoryDB) GetByID(productID int) (*Product, error) {
	query := "select id, product_name, price, product_detail, date_created from product where id = ? "
	products := Product{}
	err := r.db.Get(&products, query, productID)

	if err != nil {
		return nil, err
	}
	return &products, nil
}

func (r productRepositoryDB) GetAllProduct() ([]Product, error) {
	query := "select id, product_name, price, product_detail, date_created from product"
	products := []Product{}
	err := r.db.Select(&products, query)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (r productRepositoryDB) Update(productID int, prod Product) (*Product, error) {
	query := "UPDATE product SET product_name = ?, price = ?, product_detail = ? WHERE id = ?"
	_, err := r.db.Exec(
		query,
		prod.Product_Name,
		prod.Price,
		prod.Product_Detail,
		productID,
	)
	if err != nil {
		return nil, err
	}

	return &prod, nil
}

func (r productRepositoryDB) Delete(ProductID int) (error) {
	query := "delete from product where id = ?"
	_, err := r.db.Exec(query, ProductID)
	if err != nil {
		return nil
	}
	return nil
}
