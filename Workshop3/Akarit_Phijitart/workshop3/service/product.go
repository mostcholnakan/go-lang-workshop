package service

type ProductResponse struct {
	ID            int     `json:"id"`
	ProductName   string  `json:"product_name"`
	Price         float64 `json:"price"`
	ProductDetail string  `json:"product_detail"`
	DateCreated   string  `json:"date_created"`
}

type ProductResquest struct {
	ProductName   string  `json:"product_name"`
	Price         float64 `json:"price"`
	ProductDetail string  `json:"product_detail"`
}

type ProductService interface {
	GetProducts() ([]ProductResponse, error)
	GetProduct(int) (*ProductResponse, error)
	AddNewProduct(ProductResquest) (*ProductResponse, error)
	UpdateProduct(int, ProductResquest) (*ProductResponse, error)
	DeleteProduct(int) error
}
