package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gorm/handler"
	"gorm/repository"
	"gorm/service"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func apiDb() {

	initConfig()
	initTimeZone()
	dial := mysql.Open(fmt.Sprintf("%v@tcp(%v:%v)/%v",
		viper.GetString("db.username"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.database")))
	db, err := gorm.Open(dial, &gorm.Config{
		//Logger: &SqlLogger{},
		DryRun: false,
	})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(Customer{})

	customerRepository := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepository)
	customerHandler := handler.NewCustomerHandler(customerService)

	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*,http://localhost:5173",
		AllowMethods:     "POST, GET, OPTIONS, PUT, DELETE",
		AllowHeaders:     "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token",
		AllowCredentials: true,
	}))

	app.Get("/customers", customerHandler.GetCustomers)
	app.Get("/customers/:id", customerHandler.GetCustomer)
	app.Post("/customers", customerHandler.AddCustomer)
	app.Put("/customers/:id", customerHandler.UpdateCustomer)
	app.Delete("/customers/:id", customerHandler.DeleteCustomer)

	err = app.Listen(fmt.Sprintf(":%v", viper.GetInt("app.port")))
	if err != nil {
		log.Fatal(err)
	}

}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

type Customer struct {
	ID           int
	Name         string
	Phone_number string
	Date_created string
}

func main() {

	apiDb()
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

type SqlLogger struct {
	logger.Interface
}

func (l SqlLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowAffected int64), err error) {
	sql, _ := fc()
	fmt.Printf("%v\n=======================================\n", sql)
}
