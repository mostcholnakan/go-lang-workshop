package main

import (
	"fmt"
	"strconv"
	"workspace/P1"
	"workspace/P2"
	"workspace/P3"
	"workspace/P4"
	"workspace/P5"
	"workspace/P6"
	"workspace/SB"
)

func main() {
	fmt.Println("############### P1 ###############")
	arrayOfNumber := []float32{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}
	fmt.Println("The average of", SB.StringBuilderFloat32(arrayOfNumber), "is", P1.ToAverage(arrayOfNumber))

	fmt.Println("############### P2 ###############")
	userInput := P4.Input("Enter number:")
	num, _ := strconv.Atoi(userInput)
	fmt.Println(userInput, "is", P2.Prime(num))

	fmt.Println("############### P3 ###############")
	P3.Inputperson()
	informationMap := map[string]string{"Name": P3.Name, "Age": strconv.Itoa(int(P3.Age)), "Score": fmt.Sprintf("%v", P3.Score)}
	fmt.Println("Name:", informationMap["Name"])
	fmt.Println("Age:", informationMap["Age"])
	fmt.Println("Score:", informationMap["Score"])

	fmt.Println("############### P4 ###############")
	fmt.Println(P4.Hello())

	fmt.Println("############### P5 ###############")
	fiboInput, _ := strconv.ParseInt(P4.Input("Enter Fibonacci's lenth:"), 10, 64)
	fmt.Println(P5.Fibonacci(fiboInput))

	fmt.Println("############### P6 ###############")
	tableInput, _ := strconv.ParseInt(P4.Input("Enter table's lenth:"), 10, 64)
	P6.MultiplyTable(tableInput)
}
