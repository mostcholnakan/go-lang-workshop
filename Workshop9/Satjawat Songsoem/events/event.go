package events

import "reflect"

var Topics = []string{
	//reflect.TypeOf(GetCustomers{}).Name(),
	//reflect.TypeOf(GetCustomer{}).Name(),
	reflect.TypeOf(InsertCustomer{}).Name(),
	reflect.TypeOf(RemoveCustomer{}).Name(),
	reflect.TypeOf(UpdateCustomer{}).Name(),
}

type Event interface{

}

/*
type GetCustomers struct {
	CustomerID   int 
}

type GetCustomer struct {
	CustomerID   int   
}*/

type InsertCustomer struct {
	CustomerID   string   
	CustomerName string 
	PhoneNumber  string 
	BirthDate    string 
}

type RemoveCustomer struct {
	CustomerID   string 
}

type UpdateCustomer struct {
	CustomerID   string   
	CustomerName string 
	PhoneNumber  string 
	BirthDate    string 
}