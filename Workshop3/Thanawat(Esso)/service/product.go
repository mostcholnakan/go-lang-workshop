package service

type ProductResponse struct {
	ProductId     int    `json:"product_id"`
	Name          string `json:"name"`
	Price         string `json:"price"`
	ProductDetail string `json:"product_detail"`
	DateCreate    string `db:"date_created"`
}

type ProductService interface {
	GetProducts() ([]ProductResponse, error)
	GetProduct(int) (*ProductResponse, error)
	AddProduct(ProductResponse) (int, error)
	UpdateProduct(int, ProductResponse) error
	DeleteProduct(int) error
}
