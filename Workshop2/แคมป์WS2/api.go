package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

const currentPath = "product"
const basePath = "/api"

type Product struct {
	ID             int     `json: "id"`
	Product_Name   string  `json: "product_name"`
	Price          float64 `json: "price"`
	Product_Detail string  `json: "product_detail"`
	Date_Created   string  `json: "date_created"`
}

func SetupDB() {
	var err error
	DB, err = sql.Open("mysql", "root:08012002@tcp(127.0.0.1:3306)/coursedb")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(DB)
	log.Println("connected DB")
	DB.SetConnMaxLifetime(time.Minute * 3)
	DB.SetMaxOpenConns(10)
	DB.SetMaxIdleConns(10)
}

func creatingTable() {
	query := `CREATE TABLE product (
		id INT AUTO_INCREMENT,
		product_name TEXT NOT NULL,
		price FLOAT NOT NULL,
		product_detail TEXT NOT NULL,
		date_created DATETIME,
		PRIMARY KEY (id)
	);`

	if _, err := DB.Exec(query); err != nil {
		log.Fatal(err)
	}
}

func getProductList() ([]Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	results, err := DB.QueryContext(ctx, `SELECT
	id,
	product_name,
	price,
	product_detail,
	date_created
	FROM product`)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	defer results.Close()
	products := make([]Product, 0)
	for results.Next() {
		var product Product
		results.Scan(&product.ID, &product.Product_Name,
			&product.Price, &product.Product_Detail, &product.Date_Created)
		products = append(products, product)
	}
	return products, nil
}
func insertProduct(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	date_created := time.Now()
	result, err := DB.ExecContext(ctx, `INSERT INTO product
	(id,
	product_name,
	price,
	product_detail,
	date_created
	) VALUES (?, ?, ?, ?, ?)`,
		product.ID,
		product.Product_Name,
		product.Price,
		product.Product_Detail,
		date_created)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	insertID, err := result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	return int(insertID), nil
}
func updateProduct(product Product) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	result, err := DB.ExecContext(ctx, `UPDATE product SET 
	product_name=?, price=?, product_detail=?
	WHERE id=?`,
		product.Product_Name, product.Price,
		product.Product_Detail, product.ID)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	return rowsAffected, nil
}

func getProduct(productID int) (*Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	row := DB.QueryRowContext(ctx, `SELECT 
	id, 
	product_name, 
	price, 
	product_detail,
	date_created
	FROM product 
	WHERE id = ?`, productID)

	product := &Product{}
	err := row.Scan(
		&product.ID,
		&product.Product_Name,
		&product.Price,
		&product.Product_Detail,
		&product.Date_Created,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		log.Println(err)
		return nil, err
	}
	return product, nil
}
func removeProduct(productID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := DB.ExecContext(ctx, `DELETE FROM product where id = ?`, productID)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

func handlerProducts(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		productList, err := getProductList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(productList)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("API : localhost:5000/api/product : User Calling MethodGet For Fetch All Product : ", productList)
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var product Product
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("URL : localhost:5000/api/product/{product: %v} : Call MethodPost for posting Product With ID : %d", product, product.ID)
		productID, err := insertProduct(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"id":%d}`, productID)))
	case http.MethodOptions:
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
func handlerProduct(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, fmt.Sprintf("%s/", currentPath))
	if len(urlPathSegments[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	switch r.Method {
	case http.MethodGet:
		product, err := getProduct(productID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("URL : localhost:5000/api/product/{product: %v} : Call MethodGet for getting Product With ID : %d", product, productID)
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPut:
		var product Product
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("URL : localhost:5000/api/product/{product: %v} : Call MethodPut for updating Product With ID : %d", product, productID)
		product.ID = productID
		rowsAffected, err := updateProduct(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if rowsAffected == 0 {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
	case http.MethodDelete:
		product, err := getProduct(productID)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("URL : localhost:5000/api/product/{product: %v} : Call MethodDelete for deleting Product With ID : %s", productID, j)
		err = removeProduct(productID)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE")
		w.Header().Add("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Authorization")
		handler.ServeHTTP(w, r)
	})
}

func SetupRoutes(apiBasePath string) {
	productsHandler := http.HandlerFunc(handlerProducts)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, currentPath), corsMiddleware(productsHandler))

	productHandler := http.HandlerFunc(handlerProduct)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, currentPath), corsMiddleware(productHandler))
}

func main() {
	SetupDB()
	SetupRoutes(basePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
