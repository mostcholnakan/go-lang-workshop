package service

import (
	"log"
	"net/http"
	"time"

	"github.com/Akarit2001/workshop3/errs"
	"github.com/Akarit2001/workshop3/logs"
	"github.com/Akarit2001/workshop3/repository"
	"gorm.io/gorm"
)

const timeformate string = time.ANSIC

type productService struct {
	productRepo repository.ProductRepository
}

func NewProductService(repo repository.ProductRepository) productService {
	return productService{productRepo: repo}
}

func (s productService) GetProducts() ([]ProductResponse, error) {
	products, err := s.productRepo.GetAll()
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errs.AppError{
				Code:    http.StatusNotFound,
				Massage: "product not found",
			}
		}
		logs.Error(err)
		return nil, errs.NewUnExpectedError()
	}

	productResponses := []ProductResponse{}
	for _, product := range products {
		productResponse := ProductResponse{
			ID:            product.ID,
			ProductName:   product.ProductName,
			Price:         product.Price,
			ProductDetail: product.ProductDetail,
			DateCreated:   product.DateCreated.Format(timeformate),
		}
		productResponses = append(productResponses, productResponse)
	}
	return productResponses, nil
}

func (s productService) GetProduct(id int) (*ProductResponse, error) {
	product, err := s.productRepo.GetById(id)
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errs.NewNotfoundError("product not found")
		}
		logs.Error(err)
		return nil, errs.NewUnExpectedError()
	}
	productResponse := ProductResponse{
		ID:            product.ID,
		ProductName:   product.ProductName,
		Price:         product.Price,
		ProductDetail: product.ProductDetail,
		DateCreated:   product.DateCreated.Format(timeformate),
	}
	return &productResponse, nil
}

func (s productService) AddNewProduct(productRe ProductResquest) (*ProductResponse, error) {
	product := repository.Product{
		ID:            0,
		ProductName:   productRe.ProductName,
		ProductDetail: productRe.ProductDetail,
		Price:         productRe.Price,
		DateCreated:   time.Now(),
	}
	product2, err := s.productRepo.Create(product)
	if err != nil {
		return nil, errs.AppError{
			Code:    http.StatusInternalServerError,
			Massage: "unexpected error",
		}
	}
	productResponse := ProductResponse{
		ID:            product2.ID,
		ProductName:   product2.ProductName,
		ProductDetail: product2.ProductDetail,
		Price:         product2.Price,
		DateCreated:   product2.DateCreated.Format(timeformate),
	}
	return &productResponse, nil
}

func (s productService) UpdateProduct(pid int, productRe ProductResquest) (*ProductResponse, error) {
	product := repository.Product{
		ID:            pid,
		ProductName:   productRe.ProductName,
		ProductDetail: productRe.ProductDetail,
		Price:         productRe.Price,
	}
	product2, err := s.productRepo.Update(product)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	productResponse := ProductResponse{
		ID:            product2.ID,
		ProductName:   product2.ProductName,
		ProductDetail: product2.ProductDetail,
		Price:         product2.Price,
		DateCreated:   product2.DateCreated.Format(timeformate),
	}
	return &productResponse, nil
}

func (s productService) DeleteProduct(pid int) error {
	err := s.productRepo.Delete(pid)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
