package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"github.com/Akarit2001/workshop3/handler"
	"github.com/Akarit2001/workshop3/logs"
	"github.com/Akarit2001/workshop3/repository"
	"github.com/Akarit2001/workshop3/service"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length")

		log.Printf(" %v %v ", r.Method, r.URL.Path)
		var body []byte
		if r.Method != http.MethodGet && r.Method != http.MethodDelete {
			var err error
			body, err = ioutil.ReadAll(r.Body)
			if err != nil {
				log.Printf("Error reading request body: %v", err)
			}
			log.Printf("request body: %v", string(body))
			r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		}

		recorder := httptest.NewRecorder()

		next.ServeHTTP(recorder, r)
		resp := recorder.Result()
		// Copy the response back to the original response writer
		for key, values := range resp.Header {
			for _, value := range values {
				w.Header().Add(key, value)
			}
		}
		w.WriteHeader(resp.StatusCode)
		rbody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("Error reading response body: %v", err)
		} else {
			log.Printf("code: %v Response body: %s", resp.StatusCode, rbody)
			// Write the response body back to the original response writer
			_, err = w.Write(rbody)
			if err != nil {
				log.Printf("Error writing response body: %v", err)
			}
		}
	})
}

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()
	productRepository := repository.NewProductRepositoryDB(db)
	productService := service.NewProductService(productRepository)
	productHandler := handler.NewProductHandler(productService)

	router := mux.NewRouter()
	router.Use(middleware)
	path := "/products/{pid:[0-9]+}"
	router.HandleFunc("/products", productHandler.GetProducts).Methods(http.MethodGet)
	router.HandleFunc(path, productHandler.GetProduct).Methods(http.MethodGet)
	router.HandleFunc(path, productHandler.UpdateProduct).Methods(http.MethodPut)
	router.HandleFunc(path, productHandler.DeleteProduct).Methods(http.MethodDelete)
	router.HandleFunc("/products", productHandler.AddNewProduct).Methods(http.MethodPost)

	logs.Info("service started at port: " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetString("app.port")), router)
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

func initDatabase() *sqlx.DB {
	// "root:root@tcp(127.0.0.1:3306)/tcc_workshop?parseTime=true"
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.pasword"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.databasename"),
	)
	db, err := sqlx.Open(viper.GetString("db.driver"), dsn)
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return db
}
