package P1

func ToAverage(numArray []float32) float32 {
	var total float32
	for i := 0; i < len(numArray); i++ {
		total += numArray[i]
	}
	total = total / float32(len(numArray))
	return total
}
